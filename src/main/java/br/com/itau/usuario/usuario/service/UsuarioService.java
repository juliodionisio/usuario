package br.com.itau.usuario.usuario.service;

import br.com.itau.usuario.usuario.models.Usuario;
import br.com.itau.usuario.usuario.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario create(Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    public Usuario getById(Long id) {
        Optional<Usuario> byId = usuarioRepository.findById(id);

        /*if(!byId.isPresent()) {
            throw new UsuarioNotFoundException();
        }*/

        return byId.get();
    }
}
