package br.com.itau.usuario.usuario.repository;

import br.com.itau.usuario.usuario.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {
}
